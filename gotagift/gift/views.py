from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, HttpResponse
from django.views.generic import View

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required

from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.views import APIView

from django import forms
from gift.models import Member

from gift.serializers import MemberSerializer
from gift.forms import MemberForm


class LoginView(View):
    '''
    Login page - the first page
    '''
    def get(self, request):
        # The request is not a HTTP POST, so display the login form.
        # This scenario would most likely be a HTTP GET.
        # No context variables to pass to the template system, hence the
        # blank dictionary object...
        return render(request,"login.html",)

    def post(self, request):
        # If the request is a HTTP POST, try to pull out the relevant information.
        # Gather the username and password provided by the user.
        # This information is obtained from the login form.
        username = request.POST['username']
        password = request.POST['password']

        # Use Django's machinery to attempt to see if the username/password
        # combination is valid - a User object is returned if it is.
        user = authenticate(username=username, password=password)

        # If we have a User object, the details are correct.
        # If None (Python's way of representing the absence of a value), no user
        # with matching credentials was found.
        if user:
            # Is the account active? It could have been disabled.
            if user.is_active:
                # If the account is valid and active, we can log the user in.
                # We'll send the user back to the homepage.
                login(request, user)
                return HttpResponseRedirect('/gift/member/')
            else:
                # An inactive account was used - no logging in!
                return HttpResponse("\n\n\t\t\t:(")
        else:
            # Bad login details were provided. So we can't log the user in.
            print "Invalid login details: {0}, {1}".format(username, password)
            #return HttpResponse("Invalid login details supplied.")
            return render(request,"login.html", {'error':'invalid username/password'})


class AddMemberView(View):
    '''
    Once user logged in reaches here. ie the Dashboard
    which has a form to submit 'member' name
    '''
    #Initialize member form which is written in forms.py
    form_class = MemberForm
    #If request is GET ie not POST, then get() is called

    def get(self, request):
        #assigining form into a pointer
        form = self.form_class()
        currentuser = request.user.username
        #rendering template to display form
        return render(request,"index.html", {'form':form,'currentuser':currentuser})
    #if request is POST, post() is called
    def post(self, request):
        #assinging form to a pointer
        form = self.form_class(request.POST)
        currentuser = request.user.username
        #validating form based on forms.py
        if form.is_valid():
            #if form satisfy validation
            model_instance = form.save(commit=False)
            #saving member instance into db
            model_instance.save()
            #redirect to members list page
            return render(request,"welcome.html", {'form':form,'currentuser':currentuser})
        else:
            return render(request,"index.html", {'form':form, 'error':'invalid member name supplied','currentuser':currentuser})

class MemberView(View):
    '''
    MemberView will pass all members list to template
    '''
    #if request is GET
    def get(self, request):
        #fetch all elements from Member table and store into variable member
        members = Member.objects.all()
        currentuser = request.user.username
        #render template member.html along with members
        return render(request,"member.html", {'member':members,'currentuser':currentuser})


class APIMembersView(APIView):
    """
    APImembersView will return all members
    Authentication is needed for this methods
    """

    #validating authentication and permission
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    #if request is GET
    def get(self, request):

        # get all members list
        members = Member.objects.all()
        serializer = MemberSerializer(members, many=True)
        return Response(serializer.data)


class APIMemberView(APIView):
    """
    APImemberView will return a member with id
    Authentication is needed for this methods
    """
    #validating authentication and permission
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    #If request is GET
    def get(self,request, pk):
        try:
            #get member details
            member = Member.objects.get(id=pk)
        except Member.DoesNotExit:
            return Response('no member found',status=404)

        if request.method == 'GET':
            serializer = MemberSerializer(member)
            return Response(serializer.data)

class LogoutView(View):
    def get(self, request):
        logout(request)
        return redirect('/')



