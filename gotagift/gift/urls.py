from django.conf.urls import include, url
from django.contrib.auth.decorators import login_required
from .views import *

urlpatterns = [
    url(r'^member/$',login_required(AddMemberView.as_view())),
    url(r'^member_list/$',login_required(MemberView.as_view())),
    url(r'^api/members/$',APIMembersView.as_view()),
    url(r'^api/members/(?P<pk>[0-9]+)/$',APIMemberView.as_view()),

]
