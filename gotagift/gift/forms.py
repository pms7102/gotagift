from django import forms
from .models import Member
from django.core.validators import MinLengthValidator

class MemberForm(forms.ModelForm):
    name=forms.CharField(max_length=60, min_length=3)

    class Meta:
        model = Member
        fields = ('name', )
