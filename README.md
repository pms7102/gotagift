# README #

**You Got A Gift
**
### What is this project contains? ###

* Login Page
* Dashboard with 'Add Member' form
* Members list
* API for getting Member(s) name
* Automated Token generation for new user
* Django Administration for user/group/token/members

### How do I get set up? ###

* git clone https://pms7102@bitbucket.org/pms7102/gotagift.git
* cd gotagift
* pip install -r requirements.txt
* cd gotagift
* python manage.py migrate
* python manage.py runserver 0.0.0.0:8000


### data ###

* superuser - admin/123@admin
* member - murad
* token - admin / 4406b541347786e0d2daf2b178a4c92367b79876


### API ###

* curl -.0.1:8000/gift/api/members/ -H 'Authorization: Token 4406b541347786e0d2daf2b178a4c92367b79874'
* curl -.0.1:8000/gift/api/members/1/ -H 'Authorization: Token 4406b541347786e0d2daf2b178a4c92367b79874'

### dependencies ###

* bootstrap3
* django-rest-framework


### Who do I talk to? ###

* pms7102@gmail.com